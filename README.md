dicotomous_key

This program takes in a set of lists:

items: a list of all the items that you want to sort
criteria: a list of the criteria that is used to categorize the items

list for each criterion, with the items that fit the criterion in the list.

The program takes in these values, creates a database object for the key.
Then the database is read to create a binary tree. 
The binary tree is stored as a dot file,
and a png is rendedered for the list. 

This app is designed to run on the Google app engine using:
python3
flask
firestore
