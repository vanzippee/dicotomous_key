'use strict';

window.addEventListener('load', function () {
  document.getElementById('sign-out').onclick = function () {
    firebase.auth().signOut().then( function() {
  	location.replace("/")
	})
	
  };

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in, so display the "sign out" button and page info.
      document.getElementsByTagName("ARTICLE").hidden = false;
      document.getElementById('sign-out').hidden = false;
    } else {
      // User is signed out.
      document.getElementsByTagName("ARTICLE").hidden = true;
      location.replace("/")
    }
  }, function (error) {
    console.log(error);
    alert('Unable to log in: ' + error)
  });
});
