#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:12:12 2020
Modified: 2020-03-23

@author: Josh Baker
Description: Functions to create and access Firestore database.
"""

import json
import firebase_admin
import google
from firebase_admin import credentials, firestore

# need cred for running outside of project
# cred = credentials.Certificate('./dichotomous-key-app-4197d3079725.json')
default_app = firebase_admin.initialize_app()
db = firestore.client()


def create_doc(user, category):
    """Creates Firestore database document for user from temporary json file.
    
    Arguments:
        user {string} -- the displayname of the current user.
        category {string} -- the name of the database doc to create.
    """

    with open('/tmp/data.json', 'r') as f:
        data_dict = json.load(f)
    f.close()

    # Firestore paths alternate collection and document levels
    usr_ref = db.collection("users").document(user)
    doc_ref = usr_ref.collection("dichotomous_key").document(category)
    doc_ref.set(data_dict)
    print("Created", category, "database entry.")


def get_doc(user, category):
    """Retrieve a database document and save as a dictionary.
    
    Arguments:
        user {string} -- the displayname of the current user.
        category {string} -- the name of the database document to retrieve.
    
    Returns:
        dictionary -- dictionary of list name keys and lists of members
    """

    usr_ref = db.collection("users").document(user)
    doc_ref = usr_ref.collection("dichotomous_key").document(category)

    try:
        doc = doc_ref.get()
        full_doc = doc.to_dict()
    except google.cloud.exceptions.NotFound:
        print("Error: document not found")

    # Prints retrieved document contents to console.
    print("All members of the group:")
    for m in full_doc["members"]:
        print('\t' + m)
    print("Criteria for classification:")
    for c in full_doc["criteria"]:
        print('\t' + c)

    return full_doc


def get_db_list(user):
    """Retrives a list of all the documents for a given user.
    
    Arguments:
        user {string} -- the displaynameof the current user.
    
    Returns:
        list -- a list of database documents names.
    """

    db_list = []
    usr_ref = db.collection("users").document(user)
    doc_ref = usr_ref.collection("dichotomous_key").stream()

    for doc in doc_ref:
        db_list.append(doc)
    
    return db_list



if __name__ == "__main__":
    user = "default"
    category = "shark_families"
    create_doc(user, category)
