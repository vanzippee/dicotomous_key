#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:12:12 2020
Updated: 2020-03-23

@author: Josh Baker
Description: Main flask page for a site to create and display dichotomous keys.
"""

from flask import Flask, render_template, redirect, request, url_for
import json
from google.auth.transport import requests
import google.oauth2.id_token

from database_doc import create_doc, get_doc, get_db_list
from draw_tree import make_dichotomous_key


firebase_request_adapter = requests.Request()

app = Flask(__name__)


def write_file(entry):
    """Takes in an entry and appends it to the temporary file /tmp/file.txt
    
    Arguments:
        entry {string} -- next line written to file
    """

    with open('/tmp/file.txt', 'a') as file:
        file.write(entry)
    file.close()


@app.route('/')
def welcome():
    """Creates welcome page and sign in for user.
    Uses google firebase authentication.

    Defines string variable welcome.user that stores user display name.
    welcome.user is passed to render template. 

    Returns:
        function -- renters index.html template at route '/'
    """

    # id_token is a cookie to identify user as logged in.
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    welcome.user = ""
    heading = "Dichotomous Key Generator"

    # If the user is logged in, verify using firebase.
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(
                id_token, firebase_request_adapter)
            welcome.user = claims['name']
        except ValueError as exc:
            error_message = str(exc)
    return render_template(
        'index.html',
        user_data=welcome.user, error_message=error_message, heading=heading)


@app.route('/sample_data')
def sample_data():
    """Creates a webpage to show the user examples of how the information is
    stored in the database. This allows the user to have a better idea of 
    how they should format their imports.
    
    Returns:
        function -- Renders generic sample_form.html template, using a string
                    created from a sample database document, at route 
                    '/sample_data'. 
    """

    user = "default"  # Default user for example.
    category = "shark_families"  # Default dictionary in user profile.
    doc = get_doc(user, category)
    message = "Here are the lists used to create the database:<br><br>"
    heading = "dichotomous Key Sample Data"

    # Uses dictionary created by get_doc call to create message
    for k, v in doc.items():
        message = message + "{} = {},<br><br>".format(k, v)

    return render_template('sample_form.html', heading=heading, message=message)


@app.route('/sample_image')
def sample_image():
    """Creates a webpage to show the user an example image previously 
    created from the default database.      
    
    Returns:
        function -- Renders generic sample_form.html template - using a string
                    that contains the line reads from the dot file, and a 
                    properly formatted HTML image tag - at the route
                    '/sample_image'.
    """

    message = "Dot File:<br><br>"
    heading = "Dichotomous Key Sample Images and Files"
    image_tag = '<img src = "static/tree.png" alt="tree image" />'
    with open("static/tree.dot", "r") as dot_file:
        for line in dot_file.readlines():
            message = message + line.replace("\n", "<br>")
    message = message + "{}Sample Image:{}".format("<br>"*3, "<br>"*2)
    message = message + image_tag

    return render_template('sample_form.html', heading=heading, message=message)


@app.route("/draw_image")
def draw_image():
    """Draws an image with a call to the 'make_dichotomous key' function. 
    The resulting image is stored in the apps bucket. The bucket url and 
    the image name are then put together in an image tag that is passed as the
    message for the body of the rendered page.
    
    Returns:
        function -- Renders the generic form.html template - using the image
                    tag referencing the image location in the app bucket - at
                    route '/draw_image'. 
    """

    heading = "Dichotomous Key Image"
    img_name = welcome.user + "_" + process_key.db_name + '_tree.png'
    # img_url is specific to the google project name. 
    img_url = "https://console.cloud.google.com/storage/browser/dichotomous-key-app.appspot.com/"
    img_url = img_url.replace(" ", "%20")  # Spaces replaced for valid HTML
    img_tag = '<img_src="' + img_url + img_name + '" width = "80%">'
    message = 'Image for' + process_key.db_name + '.<br>' + img_tag

    # If the user is not logged in, the default profile is used. 
    if welcome.user == '':
        user = "default"
    else:
        user = welcome.user

    if process_key.db_name is None:
        category = "shark_families"
    else:
        category = process_key.db_name

    make_dichotomous_key(user, category)
    return render_template('form.html', heading=heading, message=message)


@app.route('/start_key', methods=["GET", "POST"])
def process_key():
    """Renders a page for the user to put in the key information - [members],
    and [criteria]. This information is written to a temporary file using the
    write_file call.
    
    Returns:
        function -- Redirects the use to route 'process_checklist' after they
                    are done. Renders the start_key.html before the form is 
                    submitted, to obtain the new key name, criteria, and
                    members.
    """

    process_key.db_name = None  # Clears previous database name.

    # If the method is POST save form data and redirect to the checklist
    if request.method == 'POST':
        process_key.db_name = request.form['category']
        process_key.members = request.form['members'].split(", ")
        process_key.criteria = request.form['criteria'].split(", ")
        entry1 = 'members = [' + ', '.join(process_key.members) + '],\n'
        entry2 = 'criteria = [' + ', '.join(process_key.criteria) + '],\n'
        entry = entry1 + entry2
        write_file(entry)
        return redirect(url_for('process_checklist'))

    # If the method is GET render the start_key page template.
    open("/tmp/file.txt", "w").close()
    return render_template('start_key.html')


@app.route("/checklist", methods=["GET", "POST"])
def process_checklist():
    """Takes the initial information provided in the /start_key page.
    The list are used as the rows and columns to create a checkbox table
    for the user to indicate which elements belong in the categories.
    
    Returns:
        function -- Renders the checklist.html template for the input
                    checklist.
    """

    return render_template("checklist.html",
                           key_name=process_key.db_name,
                           members=process_key.members,
                           criteria=process_key.criteria)


@app.route("/check_input", methods=["GET", "POST"])
def check_input():
    """Writes the checklist data to the temp doc.Displays the user inputs in
    list format for verification before the creating the database.
    
    Returns:
        function -- renders 'data_check' template for users to verify entries.
    """

    # Requests info from checklist data and write is to file
    for crit in process_key.criteria:
        entry = crit + ' = '
        criterion = request.form.getlist(crit)
        entry = entry + '[' + ', '.join(criterion) + '],\n'
        write_file(entry)
    # Opens file and displays contents on webage
    with open("/tmp/file.txt", "r") as myfile:
        output = myfile.readlines()
    myfile.close()
    data = [datum.replace('\n', '<br>') for datum in output]
    return render_template('data_check.html', heading="Check Data:",
                           message=data)


@app.route("/create_database")
def write_to_database():
    """Formats the information in the txt doc as a dictionary and writes the
    dictionary to a json file. Calls the write_doc file to create the database
    from the json file.
    
    Returns:
        function -- Renders the generic form.html template to tell the use the
                    database was successfully created. 
    """

    username = welcome.user
    category = process_key.db_name

    heading = "Dichotomous Key Generator"
    message = 'Your database "' + category + '" was created.<br>'
    link = '<a href="/draw_image">Draw your Key</a>'  # Link to make image
    message = message + link
    data_dict = {}

    with open("/tmp/file.txt", "r") as myfile:
        output = myfile.readlines()
    myfile.close()

    # Adds lines to the dictionary
    for line in output:
        entry = line.split(' = ')
        entry[1] = entry[1][1:-3].split(', ')
        data_dict[entry[0]] = entry[1]

    with open('/tmp/data.json', 'w') as outfile:
        json.dump(data_dict, outfile)
    outfile.close()

    create_doc(username, category)

    return render_template('form.html', heading=heading, message=message)


@app.route("/view_images", methods=["GET", "POST"])
def choose_img():
    """Creates a dropdown list for the user to choose any image they have
    already created. Renders a page with the image.
    
    Returns:
        function -- When method is GET a dropdown list is created.
                    When method is POST a renders a page with the image.
    """

    if request.method == 'POST':
        heading = "View Tree:"

        # Create image tag and send to template to display.
        img_name = request.form['choose_img']
        img_name = welcome.user + "_" + img_name + \
            '_tree.png?authuser=1&folder&organizationId'
        # img_url for bucket is specific to the Google project.
        img_url = "https://storage.cloud.google.com/dichotomous-key-app.appspot.com/"
        img_name = img_name.replace(" ", "%20")
        img_tag = '<img src="' + img_url + img_name + '" width = "80%">'

        return render_template("form.html", heading=heading, message=img_tag)
    # Before the image is chosen a dropdown list is created by retrieving
    # database document names.
    heading = "Choose image to view:"
    img_list = get_db_list(welcome.user)
    message = '<form action="/view_images" method="POST"><select name="choose_img">'
    for img in img_list:
        item = '<option value="' + img.id + '">' + img.id + '</option>'
        message = message + item
    message = message + '</select><input type="submit" value="Submit" /></form>'
    return render_template("form.html", heading=heading, message=message)


if __name__ == '__main__':
    app.run(debug=True)
