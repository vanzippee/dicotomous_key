#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:12:12 2020
Updated: 2020-03-23

@author: Josh Baker
Description: Uses a Google Firebase database doc to create a binary tree
representing a dichotomous key. 
"""

from database_doc import get_doc
from google.cloud import storage
from anytree.exporter import DotExporter
import pydot


class node:
    """Individual node in binary tree
        members -- members of whole that fit preceding criteria.
        values -- value property same as members, required for pydot
        criterion_index -- index refering to element in criteria list
        parent -- reference to parent node
        left/right_child -- reference to child nodes
        children -- list of child nodes (required for pydot)
        self.name -- records name of criterion
        self.x/y -- list of members that fit or do not fit criterion
    """

    def __init__(self, members=None, criterion_index=0):
        self.members = members
        self.values = members
        self.criterion_index = criterion_index
        self.parent = None
        self.left_child = None
        self.right_child = None
        self.children = []
        self.name = 'None'
        self.y = []
        self.n = []


class tree:
    """Tree object that stores the relational data between nodes.
        root -- root node (at every node it is considered the root of the
                subtree it is the root of.)
        criteria -- a complete list of the criteria
        doc -- a copy of the complete database doc for the tree
    
    Returns:
        nodes -- child nodes for current node
    """

    def __init__(self, criteria, doc):
        self.root = None
        self.criteria = criteria
        self.doc = doc

    def inspect(self, cur_node):
        """Define a Preorder traversal of the tree to inspect nodes.
        
        Arguments:
            cur_node {node object} -- current root node of subtree
        """

        if cur_node is None:  # basecase
            cur_node = node(self.doc['members'])
            self.root = cur_node
        self._inspect(cur_node)
        self._inspect(cur_node.left_child)
        self._inspect(cur_node.right_child)

    def _inspect(self, cur_node):
        """Inspects nodes to determine if they are leaf nodes. 
        In order to be a leaf node they must contain only one member.
        
        Arguments:
            cur_node {node object} -- current root node of subtree
        """

        if cur_node.left_child is None and cur_node.right_child is None:
            if len(cur_node.members) == 1:
                cur_node.name = cur_node.members[0]
            else:
                self._sort_members(cur_node)

    def _sort_members(self, cur_node):
        """Evaluate members in a node based on current criterion. If the
        criterion results in separation of members new child nodes are made.
        If not, the criterion index is advanced. 
        
        Arguments:
            cur_node {node object} -- current root node of subtree
        
        Returns:
            function -- creates child nodes
        """

        # Make two lists y[fit criterion], n[don't fit criterion]
        # If both lists have members, then members are separated into child
        # nodes.
        while cur_node.criterion_index < len(self.criteria):
            cur_node.y = []
            cur_node.n = []
            for member in cur_node.members:
                if member in self.doc[self.criteria[cur_node.criterion_index]]:
                    cur_node.y.append(member)
                else:
                    cur_node.n.append(member)
            cur_node.criterion_index += 1
            if len(cur_node.y) > 0 and len(cur_node.n) > 0:
                cur_node.name = self.criteria[cur_node.criterion_index - 1]
                return self._create_children(cur_node)

    def _create_children(self, cur_node):
        """Creates new nodes for the left and right children of the current
        node.
        
        Arguments:
            cur_node {node object} -- current root node of subtree
        """

        cur_node.left_child = node(cur_node.y, cur_node.criterion_index)
        cur_node.left_child.parent = cur_node
        cur_node.right_child = node(cur_node.n, cur_node.criterion_index)
        cur_node.right_child.parent = cur_node
        cur_node.children = [cur_node.left_child, cur_node.right_child]
        # inspect newly created nodes.
        self._inspect(cur_node.left_child)
        self._inspect(cur_node.right_child)

    def print_tree(self):
        """Prints tree nodes in order. 
        Non-recursive function. 
        """

        if self.root is not None:
            self._print_tree(self.root)

    def _print_tree(self, cur_node):
        """Prints tree nodes in order.
        Recursive function.
        
        Arguments:
            cur_node {node object} -- current root node of subtree
        """

        if cur_node is not None:
            self._print_tree(cur_node.left_child)
            if len(cur_node.members) == 1:
                print("\nNode:", cur_node.members)
            self._print_tree(cur_node.right_child)


def save_image(user, category):
    """Uses the temporary image saved at /tmp/tree.png and stores it in the
    app bucket with the name [username]_[category]_tree.png.
    
    Arguments:
        user {string} -- the displayname of the current user.
        category {string} -- the name of the database used to create the tree.
    """

    # bucket_name is specific to the Google project
    bucket_name = "dichotomous-key-app.appspot.com"
    source_file_name = "/tmp/tree.png"
    destination_blob_name = user + "_" + category + "_tree.png"

    # Create Google storage client to interact with api
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print("File {} uploaded to {}".format(source_file_name,
          destination_blob_name))


def make_dichotomous_key(user, category):
    """Creates the dot file using DotExporter, and a png using anytree.
    png image is saved as a temp file at /tmp/tree.png and a call to save_image
    saves the file to the project bucket. 
    
    Arguments:
        user {string} -- the displayname of the current user.
        category {string} -- the name of the database used to create the tree.
    """

    doc = get_doc(user, category)
    criteria = doc["criteria"]
    dichotomous_key = tree(criteria, doc)
    dichotomous_key.inspect(dichotomous_key.root)
    DotExporter(dichotomous_key.root).to_dotfile("/tmp/tree.dot")
    (graph,) = pydot.graph_from_dot_file('/tmp/tree.dot')
    graph.write_png('/tmp/tree.png')
    save_image(user, category)


if __name__ == "__main__":
    user = "default"
    category = "shark_families"
    make_dichotomous_key(user, category)
